const express = require('express')
const { div } = require('./mylib')
const app = express()
const port = 3000
const mylib = require('./mylib')

app.get('/', (req, res) => {
  res.send('Hello World!')
})
// endpoint localhost:3000/add?a=42&b=21
app.get('/add', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    console.log({ a , b })
    const total = mylib.sum(a,b)
    const division = mylib.div(a,b)
    res.send("add works "+ total.toString()+" division works "+ division.toString())
})
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})